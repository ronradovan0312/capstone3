import { useContext, useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CreateOrderModal({ product_id }) {
    const { user } = useContext(UserContext);

    const [productId, setProductId] = useState('');
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [quantity, setQuantity] = useState(0);
    const [showCreateOrderModal, setShowCreateOrderModal] = useState(false);

    const openCreateOrderModal = (productId) => {
        fetch(`https://capstone2-easypc.onrender.com/api/products/${productId}`)
            .then(response => response.json())
            .then(result => {
                setProductId(result._id);
                setName(result.name);
                setDescription(result.description);
                setPrice(result.price);
                setQuantity(0);
            });

        setShowCreateOrderModal(true);
    };

    const closeCreateOrderModal = () => {
        setShowCreateOrderModal(false);
        setProductId('');
        setName('');
        setDescription('');
        setPrice('');
        setQuantity(0);
    };

    const createOrder = (event, id) => {
        event.preventDefault();
    
        fetch(`https://capstone2-easypc.onrender.com/api/products/${id}`)
            .then(response => response.json())
            .then(product => {
                const productPrice = product.price;
                const totalAmount = productPrice * parseInt(quantity, 10); // Calculate total amount
    
                fetch(`${process.env.REACT_APP_API_URL}/api/orders/create-order`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`,
                    },
                    body: JSON.stringify({
                        products: [
                            {
                                productId: id,
                                quantity: parseInt(quantity, 10),
                            },
                        ],
                        totalAmount: totalAmount,
                    }),
                })
                    .then(response => response.json())
                    .then(result => {
                        if (result.message === 'Order created successfully') {
                            Swal.fire({
                                title: 'Order Created',
                                text: result.message,
                                icon: 'success',
                            });
    
                            // Close the modal after successful order creation
                            closeCreateOrderModal();
                        } else {
                            Swal.fire({
                                title: 'Order Creation Failed',
                                text: 'Please try again',
                                icon: 'error',
                            });
                        }
                    })
                    .catch(error => {
                        console.error('Error creating order:', error);
                        Swal.fire({
                            title: 'Error',
                            text: 'An error occurred while creating the order. Please try again.',
                            icon: 'error',
                        });
                    });
            });
    };
    

    return (
        <>
            <Button variant="outline-dark" className="m-2" onClick={() => openCreateOrderModal(product_id)}>
                Order
            </Button>

            {/* Create Order Modal */}
            <Modal show={showCreateOrderModal} onHide={closeCreateOrderModal}>
                <Form onSubmit={event => createOrder(event, productId)}>
                    <Modal.Header closeButton>
                        <Modal.Title> Create Order </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId="productName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                value={name}
                                readOnly
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="productDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                type="text"
                                value={description}
                                readOnly
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="productPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                type="number"
                                value={price}
                                readOnly
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="orderQuantity">
                            <Form.Label>Quantity</Form.Label>
                            <Form.Control
                                type="number"
                                value={quantity}
                                onChange={event => setQuantity(event.target.value)}
                                required
                            />
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeCreateOrderModal}>
                            Close
                        </Button>
                        <Button variant="success" type="submit">
                            Submit
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}
