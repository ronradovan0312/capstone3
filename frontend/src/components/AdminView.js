import { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import ArchiveProduct from './ArchiveProduct.js';
import EditProduct from './EditProduct.js';



export default function AdminView({productsData, fetchProducts}){
	const [products, setProducts] = useState([])

	useEffect(() => {
		const products_array = productsData.map(product => {
			return(
				<tr key={product._id}>
					<td> {product._id} </td>
					<td> {product.name} </td>
					<td>{ product.description} </td>
					<td> {product.price} </td>
					<td>{product.quantity}</td>
					<td> {product.isActive ? 'Available' : 'Unavailable'} </td>
					<td>
						<EditProduct 
						product_id={product._id} 
						fetchProducts={fetchProducts}
						/>
					</td>
					<td>
					<ArchiveProduct 
						product_id={product._id} 
						fetchProducts={fetchProducts} 
						isActive={product.isActive}
						/>

					</td>
				</tr>
			)
		})

		setProducts(products_array);
	},[productsData])

	return(
		<>	
			<h1 className="NavbarBrand-text text-center p-5" > Admin Dashboard </h1>
			<h3 className=" text-center"> Products </h3>
			<Table striped bordered hover responsive>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th> Stocks </th>
						<th>Availability</th>
						<th colSpan={2} className='text-center'>Actions</th>
					</tr>
				</thead>

				<tbody>
					{products}
				</tbody>
			</Table>
		</>
	)
}