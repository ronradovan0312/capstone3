import { Table } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function AdminViewOrders({ ordersData }) {
  return (
    <>
      <h1 className="NavbarBrand-text text-center p-5">Admin Dashboard</h1>
      <h2 className="text-center">Orders</h2>
      <Table striped bordered hover responsive size='sm'>
        <thead>
          <tr>
            <th className="text-center">Order ID</th>
            <th className="text-center">Customer ID</th>
            <th className="text-center">Customer Name</th>
            <th className="text-center">Mobile No.</th>
            <th className="text-center">Purchased On</th>
            <th className="text-center">Product Details</th>
            <th className="text-center">Total Amount</th>
          </tr>
        </thead>
        <tbody>
          {ordersData.map(order => (
            <tr key={order._id}>
              <td>{order._id}</td>
              <td>{order.userId}</td>
              <td>{order.userName}</td>
              <td>{order.userMobileNo}</td>
              <td>{order.purchasedOn}</td>
              <td>
                <ul>
                  {order.products.map(product => (
                    <li key={product.productId}>
                      Product: {product.productName} - Price: {product.productPrice} - Quantity: {product.quantity}
                    </li>
                  ))}
                </ul>
              </td>
              <td>{order.totalAmount}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}

AdminViewOrders.propTypes = {
  ordersData: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      userId: PropTypes.string.isRequired,
      userName: PropTypes.string.isRequired,
      userMobileNo: PropTypes.number.isRequired,
      purchasedOn: PropTypes.string.isRequired,
      products: PropTypes.arrayOf(
        PropTypes.shape({
          productId: PropTypes.string.isRequired,
          productName: PropTypes.string.isRequired,
          productPrice: PropTypes.number.isRequired,
          quantity: PropTypes.number.isRequired,
        })
      ).isRequired,
    })
  ),
};