import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function SetAsAdmin({user_id, fetchUsers, isAdmin}) {
	

	const removeAsAdmin = (userId) => {
		fetch(`https://capstone2-easypc.onrender.com/api/users/${userId}/RemoveAsAdmin`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result === false){
				Swal.fire({
					title: 'User has been removed as Admin',
					icon: 'success'
				})
				fetchUsers()
			} else {
				Swal.fire({
					title: 'Something went wrong',
					text: 'PLease try again',
					icon: 'error'
				})
				fetchUsers();
			}
		})
	}
	

	const setAsAdmin = (userId) => {
		fetch(`https://capstone2-easypc.onrender.com/api/users/${userId}/AsAdmin`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				Swal.fire({
					title: 'User has been set as Admin',
					icon: 'success'
				})
				fetchUsers()
			} else {
				Swal.fire({
					title: 'Somthing wen wrong',
					text: 'PLease try again',
					icon: 'error'
				})
				fetchUsers();
			}
		})
	}


	return(
		<>
			{isAdmin ?
				<Button 
					variant='warning' 
					size="sm"
					onClick={() => removeAsAdmin(user_id)}> Remove 
				</Button>
			:
				<Button 
				variant='success' 
				size='sm'
				onClick={() => setAsAdmin(user_id)}> admin
				</Button>
			}
		</>
	)
}