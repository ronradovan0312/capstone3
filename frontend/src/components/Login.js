import { Form, Button, Modal} from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import UserContext from '../UserContext.js';
import { useModalContext } from '../ModalContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {
	// W're able to access the 'user' state from App.js through the use of react context/provider.
	const {user, setUser} = useContext(UserContext);
	const { showLoginModal, closeModal } = useModalContext();




	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

  function loginUser(event) {
    event.preventDefault();

    fetch(`https://capstone2-easypc.onrender.com/api/users/login`, {

      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      })
    })
      .then(response => response.json())
      .then(result => {
      	if(result.message === 'your password is incorrect'){
      		 Swal.fire({
	        	title: 'Password is incorrect',
	        	text: 'Please try again',
	        	icon: 'warning'
	        })
      	}
      	else if(result.message === `The user isn't register yet.`) {
      		 Swal.fire({
	        	title: 'User does not exist',
	        	text: 'Please try again',
	        	icon: 'warning'
	        })
      	}
        else if (result.accessToken) {
        	localStorage.setItem('token', result.accessToken);
        	localStorage.setItem('userId', result.userId);

        	retrieveUserDetails(result.accessToken, result.userId)

        	setEmail("");
    			setPassword("");

	          // Handle successful login, e.g. set user session
	        Swal.fire({
	        	title: 'Loggen in Success',
	        	text: 'You have Loggen in Succefully',
	        	icon: 'success'
	        })
	        // console.log(result.accessToken); // Log the token to the console
	        // console.log(result.userId);
	        closeModal();

        } else {

          	Swal.fire({
		        	title: 'Something went wrong',
		        	text: `${email} does not exist`,
		        	icon: 'warning'
	        })
        }
      })
    }


	const retrieveUserDetails = (token, userId) => {
		fetch(`https://capstone2-easypc.onrender.com/api/users/details`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				_id: userId
			})
		})
		.then(response => response.json())
		.then(result => {
			// Once it gets the user details, we will set the global user state to have the ID and IsAdmin properties of the user who is Logged in. 
			setUser({
					id: result._id,
					isAdmin: result.isAdmin
			})		
		})
} 


  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (

				(user.id !== null) ?
				<Navigate to='/products'/>
			:


		   <Modal show={showLoginModal} onHide={closeModal}>
					    <Form onSubmit={(event) => loginUser(event)}>
					     		<Modal.Header closeButton>
													<Modal.Title>Login </Modal.Title>
									</Modal.Header>

									<Modal.Body>
		                    <Form.Group>
		                        <Form.Label>Email</Form.Label>
		                        <Form.Control 
			                        type="email" 
			                        placeholder="Email" 
			                        value={email}
			                        onChange={event => setEmail(event.target.value)}
			                        required
		                        />
		                    </Form.Group>


		                    <Form.Group>
		                        <Form.Label>Password</Form.Label>
		                        <Form.Control 
			                        type="password" 
			                        placeholder="Password" 
			                        value={password}
			                        onChange={event => setPassword(event.target.value)}
			                        required
		                        />
		                    </Form.Group>
								</Modal.Body>

								<Modal.Footer>
									<Button variant='success' type="submit" disabled = { isActive == false }> Login  </Button>
								</Modal.Footer>
					    </Form>
			   	</Modal>
			 
		
  	)
}