import { Card, Carousel, Col, Row } from 'react-bootstrap';
import myImage3 from '../images/guitar1.png';
import myImage2 from '../images/guitar2.png';
import myImage4 from '../images/guitar3.png';
import myImage5 from '../images/guitar4.png';
import myImage6 from '../images/guitar5.png';
import myImage7 from '../images/guitar6.png';
import myImage from '../images/profile.png';


export default function Highlights() {
	return (
	
		<Row> 
			<Col xs={12} md={4} className='my-2'> 
				<Card className="cardHighlight p-3 carousel-control">
				 <Carousel slide={false}>
				      <Carousel.Item>
				        <img 
				        src={myImage}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage2}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage3}
				        width = "450"
				        height = "450"
				        className="d-inline-block w-100 h-50 carousel-control"
				        />
				      </Carousel.Item>
				    </Carousel>	
				</Card>			  
			</Col>

			<Col xs={12} md={4} className='my-2'> 
				<Card className="cardHighlight p-3 carousel-control">
				<Carousel slide={false}>
				      <Carousel.Item>
				        <img 
				        src={myImage}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage4}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage5}
				        width = "450"
				        height = "450"
				        className="d-inline-block w-100 h-50 carousel-control"
				        />
				      </Carousel.Item>
				    </Carousel>	
				 </Card>			 
			</Col>

			<Col xs={12} md={4} className='my-2'> 
				<Card className="cardHighlight p-3 carousel-control">
				<Carousel slide={false}>
				      <Carousel.Item>
				        <img 
				        src={myImage}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage6}
				        width = "450"
				        height = "450"
				        className="d-inline-block  w-100 carousel-control"
				        />
				      </Carousel.Item>

				      <Carousel.Item>
				       <img 
				        src={myImage7}
				        width = "450"
				        height = "450"
				        className="d-inline-block w-100 h-50 image-fluid carousel-control"
				        />
				      </Carousel.Item>
				    </Carousel>	
				 </Card>			   
			</Col>			
		</Row>
	)
	
	
}