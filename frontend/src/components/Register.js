import { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useModalContext } from '../ModalContext';

export default function Register() {
	const { showRegisterModal, closeModal } = useModalContext();

	const[firstName, setFirstName] = useState("");
	const[lastName, setLastName] = useState("");
	const[email, setEmail] = useState("");
	const[mobileNo, setMobileNo] = useState(0);
	const[password, setPassword] = useState("");
	const[confirmPassword, setConfirmPassword] = useState("");



	function registerUser(event) {
		event.preventDefault();


		fetch(`https://capstone2-easypc.onrender.com/api/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(response => response.json())
		.then(result => {

			if(result.message === "Email address is already registered"){

				Swal.fire({
					title: 'Email is already used',
					text: result.message,
					icon: 'warning'
				})
			} else if(result){
				setFirstName("")
				setLastName("")
				setEmail("")
				setMobileNo("")
				setPassword("")
				setConfirmPassword("")

				closeModal();
				Swal.fire({
					title: 'Successfully Registered',
					text: result.message,
					icon: 'success'
				})
			} else {
				Swal.fire({
					title: 'Something went wrong',
					text: 'Please try again',
					icon: 'error'
				})
			}

		})
	}

	return(
		<Modal show={showRegisterModal} onHide={closeModal}>
			<Form onSubmit = {(event) => registerUser(event)}>
				<Modal.Header closeButton>
					<Modal.Title> Signup
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form.Group>
                        <Form.Label>First Name</Form.Label>
                        <Form.Control 
	                        type="text" 
	                        placeholder="First Name" 
	                        value={firstName}
	                        onChange={event => setFirstName(event.target.value)}
	                        required
                        />
	                </Form.Group>

                    <Form.Group >
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control 
	                        type="text" 
	                        placeholder="Last Name" 
	                        value={lastName}
	                        onChange={event => setLastName(event.target.value)}
	                        required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control 
	                        type="email" 
	                        placeholder="Email" 
	                        value={email}
	                        onChange={event => setEmail(event.target.value)}
	                        required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label> Contact / Mobile No:.</Form.Label>
                        <Form.Control 
	                        type="number"
	                        placeholder="Mobile Number" 
	                        value={mobileNo}
	                        onChange={event => setMobileNo(event.target.value)}
	                        required
                        />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
	                        type="password" 
	                        placeholder="Password" 
	                        value={password}
	                        onChange={event => setPassword(event.target.value)}
	                        required
                        />
                    </Form.Group>

                     <Form.Group>
		                <Form.Label>Confirm Password:</Form.Label>
		                <Form.Control 
			                type="password" 
			                placeholder="Confirm Password" 
			                value ={confirmPassword}
			                onChange={event => {setConfirmPassword(event.target.value)}}
			                required/>
	            	</Form.Group>



				</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={closeModal}> Close </Button>
					<Button variant='success' type="submit"> Submit </Button>
				</Modal.Footer>
			</Form>
		</Modal>


	)

}

