import { useContext } from 'react';
import { Container, Nav, NavDropdown, Navbar } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { useModalContext } from '../ModalContext.js';
import UserContext from '../UserContext.js';
import myImage from '../images/guitarLogo1.png';

export default function AppNavbar() {
	const { user } = useContext(UserContext);
	 const { openModal, closeModal } = useModalContext();

	return (
		<Navbar expand="lg" id="nav-bar" className="bg-* sticky-top">
	      <Container>
	        <Navbar.Brand className="NavbarBrand-text" href="/"> 
	        <img
              alt=""
              src={myImage}
              width="40"
              height="40"
              className="d-inline-block align-top"
            />{' '}
            Thift and Stuff 
            </Navbar.Brand>

	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="ms-auto">

	            <Nav.Link 
	            className="NavbarBrand-text" 
	            as={NavLink} to='/'>
	            Home
	            </Nav.Link>

	            <Nav.Link 
	            className="NavbarBrand-text"
	            as={NavLink} to='/products'>
	            Products 
	            </Nav.Link>

	            {(user.id !== null) ?
	            	   user.isAdmin ?
		            	   	<>
		            	   		
			            	   	<NavDropdown 
			            	   	className="NavbarBrand-text"
			            	   	title="Menu" 
			            	   	id="basic-nav-dropdown">

					            <NavDropdown.Item 
					            className="NavbarBrand-text"
					            onClick={() => 
					            openModal('addProduct')}>
					              	Add Product
					            </NavDropdown.Item>

					            <NavDropdown.Item 
					            className="NavbarBrand-text"
					            as={NavLink}
								to='/users'>
					              	View users
					            </NavDropdown.Item>

					            <NavDropdown.Item 
					            className="NavbarBrand-text"
					            as={NavLink}
								to='/orders'>
					                View orders
					            </NavDropdown.Item>

					            <NavDropdown.Divider />

					             <NavDropdown.Item 
					             className="NavbarBrand-text"
					             as={NavLink}
					             to='/logout'>
					             Logout
					            </NavDropdown.Item>

			           			</NavDropdown>
		            	   	</>
		            	 :
		            	 	<>
			            	   	<NavDropdown 
			            	   	className="NavbarBrand-text"
			            	   	title="Menu" 
			            	   	id="basic-nav-dropdown">

					            <NavDropdown.Item 
					            className="NavbarBrand-text"
					            href="#action/3.1">
					              	View orders
					            </NavDropdown.Item>

					            <NavDropdown.Item 
					            className="NavbarBrand-text"
					            as={NavLink}
					             to='/cart'>
					                View cart
					            </NavDropdown.Item>

					            <NavDropdown.Divider />

					             <NavDropdown.Item 
					             className="NavbarBrand-text"
					             as={NavLink}
					             to='/logout'>
					               Logout 
					            </NavDropdown.Item>

			           			</NavDropdown>
		            	   	</>
		           :
		           
		           <NavDropdown 
			    		 className="NavbarBrand-text"
			    		 title="GetStarted" 
			    		 id="basic-nav-dropdown">

				              <NavDropdown.Item 
				              className="NavbarBrand-text" 
				              onClick={() => openModal('signup')}>
				              	Signup
				              </NavDropdown.Item>

				              <NavDropdown.Item 
				              className="NavbarBrand-text" 
				              onClick={() => 
				              openModal('login')}>
				                Login
				              </NavDropdown.Item>

			            </NavDropdown>
	        	
	           }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
    </Navbar>
	)
}