import { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddToCart({product_id, fetchUsers}) {

	// This states will contain the ID of the product that is selected
	const [productId, setProductId] = useState('');

	// Form states
	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('');
    const [quantity, setQuantity] = useState(0)

	// For modal activation 
	const [showAddToCartModal, setShowAddToCartModal] = useState(false);

	const openAddToCartModal = (productId) => {
		fetch(`https://easypc-capstone2.onrender.com/api/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			// Pre-populate the form input fields with data from the API
			setProductId(result._id)
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
            setQuantity(0)
		})

		// Then, open the Modal
		setShowAddToCartModal(true);

	}

	const closeAddToCartModal = () => {
		setShowAddToCartModal(false);
		setName('')
		setDescription('')
		setPrice('')
        setQuantity('')
	}

    const addToCart = (event, id) => {
        event.preventDefault();
    
        fetch(`https://capstone2-easypc.onrender.com/api/products/${id}`) // Fetch product details
            .then(response => response.json())
            .then(product => {
                const productPrice = product.price;
                const totalAmount = productPrice * parseInt(quantity, 10); // Calculate total amount
    
                fetch(`https://capstone2-easypc.onrender.com/api/users/add-to-cart`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`,
                    },
                    body: JSON.stringify({
                        productId: id,
                        quantity: parseInt(quantity, 10),
                        totalAmount: totalAmount, // Include total amount in the request
                    }),
                })
                    .then(response => response.json())
                    .then(result => {
                        if (result.message === 'Product added to cart') {
                            // ... your existing code
                            Swal.fire({
                                title: "Successfully added to cart",
                                text: result.message,
                                icon: 'success'
                            })
            
                            // To redirect back to the courses page after adding to cart
                            
                            closeAddToCartModal()
                        } else {
                            // ... your existing code
                            Swal.fire({
                                title: "Something Went wrong",
                                text: 'Please try again',
                                icon: 'error'
                        })
                        }
                    });
            });
    };

	return(
		<>
		<Button variant="outline-dark" className='m-2'  onClick={() => openAddToCartModal(product_id)}> Add to Cart </Button>

		{/*Add to Cart Modal*/}

		<Modal show={showAddToCartModal} onHide={closeAddToCartModal}>
			<Form onSubmit={event => addToCart(event, productId)}>
				<Modal.Header closeButton>
					<Modal.Title> Add to cart </Modal.Title>
				</Modal.Header>
				<Modal.Body>

					<Form.Group controlId="productName">
                        <Form.Label>Name</Form.Label>
                        <Form.Control 
	                        type="text" 
	                        value={name}
	                        onChange={event => setName(event.target.value)}
	                        required
                        />
	                </Form.Group>

                    <Form.Group controlId="productDescription">
                        <Form.Label>Description</Form.Label>
                        <Form.Control 
	                        type="text" 
	                        value={description}
	                        onChange={event => setDescription(event.target.value)}
	                        required
                        />
                    </Form.Group>

                    <Form.Group controlId="coursePrice">
                        <Form.Label>Price</Form.Label>
                        <Form.Control 
	                        type="number" 
	                        value={price}
	                        onChange={event => setPrice(event.target.value)}
	                        required
                        />
                    </Form.Group>

                    <Form.Group controlId="coursePrice">
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control 
	                        type="number" 
	                        value={quantity}
	                        onChange={event => setQuantity(event.target.value)}
	                        required
                        />
                    </Form.Group>
				</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={closeAddToCartModal}> Close </Button>
					<Button variant='success' type="submit"> Submit </Button>
				</Modal.Footer>
			</Form>
		</Modal>
		</>
	)
}