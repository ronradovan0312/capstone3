// import PropTypes from 'prop-types';
// import React, { useContext } from 'react';
// import { Button, Card, Col, Container, Row } from 'react-bootstrap';
// import Swal from 'sweetalert2';
// import UserContext from '../UserContext';

// export default function ViewCart({ cartData }) {
//     const { user } = useContext(UserContext);

//     const handleRemoveFromCart = (productId) => {
//         if (!user.id) {
//             Swal.fire({
//                 title: 'Login required',
//                 text: 'Please log in to remove items from your cart.',
//                 icon: 'warning',
//             });
//             return;
//         }

//         // Remove the item from the cartData array instead of setting it in state
//         const updatedCartData = cartData.filter(item => item.product._id !== productId);

//         fetch(`${process.env.REACT_APP_API_URL}/api/users/remove-from-cart/${productId}`, {
//             method: 'DELETE',
//             headers: {
//                 'Content-Type': 'application/json',
//                 Authorization: `Bearer ${localStorage.getItem('token')}`,
//             },
//         })
//             .then(response => response.json())
//             .then(result => {
//                 if (result.message === 'Product removed from cart') {
//                     Swal.fire({
//                         title: 'Product removed',
//                         text: 'The product has been removed from your cart.',
//                         icon: 'success',
//                     });
//                 } else {
//                     Swal.fire({
//                         title: 'Error',
//                         text: 'Something went wrong. Please try again.',
//                         icon: 'error',
//                     });
//                 }
//             })
//             .catch(error => {
//                 console.error('Error removing product from cart:', error);
//             });
//         localStorage.setItem('cartData', JSON.stringify(updatedCartData));
//     };

//     return (
//         <Container>
//             <h2 className="my-4">Your Cart</h2>
//             {cartData.length === 0 ? (
//                 <p>Your cart is empty.</p>
//             ) : (
//                 <Row xs={1} md={2} lg={3} className="g-4">
//                     {cartData.map((item, index) => (
//                         <Col key={index}>
//                             <Card>
//                                 <Card.Img variant="top" src={item.product.image} />
//                                 <Card.Body>
//                                     <Card.Title>{item.product.name}</Card.Title>
//                                     <Card.Text>
//                                         Quantity: {item.quantity}
//                                         <br />
//                                         Subtotal: Php {item.subtotal}
//                                     </Card.Text>
//                                     <Button
//                                         variant="danger"
//                                         onClick={() => handleRemoveFromCart(item.product._id)}
//                                     >
//                                         Remove from Cart
//                                     </Button>
//                                 </Card.Body>
//                             </Card>
//                         </Col>
//                     ))}
//                 </Row>
//             )}
//         </Container>
//     );
// }

// ViewCart.propTypes = {
//     cartData: PropTypes.array.isRequired,
// };
