import { useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function EditCourse({product_id, fetchProducts}) {
	// This states will contain the ID of the product that is selected
	const [productId, setProductId] = useState('');

	// Form statesS
	const [image, setImage] = useState();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState('');

	// For modal activation 
	const [showEditModal, setShowEditModal] = useState(false);

	const openEditModal = (productId) => {
		fetch(`https://capstone2-easypc.onrender.com/api/products/${productId}`)
		.then(response => response.json())
		.then(result => {
			// Pre-populate the form input fields with data from the API
			setProductId(result._id)
			setImage(result.image)
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
			setQuantity(result.quantity)
		})

		// Then, open the Modal
		setShowEditModal(true);

	}

	const closeEditModal = () => {
		setShowEditModal(false);
		setImage()
		setName('')
		setDescription('')
		setPrice('')
		setQuantity('')
	}


	const editProduct = (event, productId) => {
		event.preventDefault();

		let token = localStorage.getItem('token')

		fetch(`https://capstone2-easypc.onrender.com/api/products/${productId}`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				image: image,
				name: name,
				description: description,
				price: price,
				quantity: quantity
			})
		})
		.then(response => response.json())
		.then(result => {
			if(result){
				Swal.fire({
		            title: 'Product Updated',
		            text: result.message,
		            icon: 'success',
          		})

				// Triggers the fetching of all courses after updating a course. The 'fetchCourses' function comes from the props that were passed from the Products.js cmponent to the adminView.js component and finally to this EditCourse.js
           		fetchProducts();
          		closeEditModal();
			} else {
				Swal.fire({
		            title: 'Something went wrong',
		            text: 'Please try again',
		            icon: 'error',
		        });
		        fetchProducts();
		        closeEditModal();
			}
		})

	}



	return(
		<>
		<Button variant="primary" size="sm"  onClick={() => openEditModal(product_id)}> Edit </Button>

		{/*Edit Modal*/}

		<Modal show={showEditModal} onHide={closeEditModal}>
			<Form onSubmit={event => editProduct(event, productId)}>
				<Modal.Header closeButton>
					<Modal.Title> Edit Course </Modal.Title>
				</Modal.Header>
				<Modal.Body>

					<Form.Group>
                        <Form.Label>Url for image:</Form.Label>
                        <Form.Control 
                        type="url" 
                        placeholder="Enter url" 
                        value={image || ''}  
						onChange={event => setImage(event.target.value)}
						required
						/>
                    </Form.Group>

					<Form.Group controlId="productName">
                        <Form.Label>Name</Form.Label>
                        <Form.Control 
	                        type="text" 
	                        value={name}
	                        onChange={event => setName(event.target.value)}
	                        required
                        />
	                </Form.Group>

                    <Form.Group controlId="productDescription">
                        <Form.Label>Description</Form.Label>
                        <Form.Control 
	                        type="text" 
	                        value={description}
	                        onChange={event => setDescription(event.target.value)}
	                        required
                        />
                    </Form.Group>

                    <Form.Group controlId="productPrice">
                        <Form.Label>Price</Form.Label>
                        <Form.Control 
	                        type="number" 
	                        value={price}
	                        onChange={event => setPrice(event.target.value)}
	                        required
                        />
                    </Form.Group>

					<Form.Group controlId="productQuantity">
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control 
	                        type="number" 
	                        value={quantity}
	                        onChange={event => setQuantity(event.target.value)}
	                        required
                        />
                    </Form.Group>

				</Modal.Body>
				<Modal.Footer>
					<Button variant='secondary' onClick={closeEditModal}> Close </Button>
					<Button variant='success' type="submit"> Submit </Button>
				</Modal.Footer>
			</Form>
		</Modal>
		</>
	)
}