import { Carousel, Col, Container, Row } from 'react-bootstrap';
import myImage from '../images/Banner1.png';
import myImage2 from '../images/Banner2.png';
import myImage3 from '../images/Banner3.png';

export default function Banner() {
	return(
	
 		<Container fluid className="ban1">
	 			<Row>
	 				<Col xs={12} md={12}>
	 					<Carousel slide={false} >
					      <Carousel.Item className="ban2">
					      	<img 
						        src={myImage}
						        width = "2040"
						        height = "608"
						        className="d-inline-block w-100 image-fluid"
						        />
					      </Carousel.Item>

					      <Carousel.Item className="ban2">
					        <img 
						        src={myImage2}
						        width = "2040"
						        height = "608"
						        className="d-inline-block w-100 image-fluid"
						        />
					      
					      </Carousel.Item>

					      <Carousel.Item className="ban2">
					       <img 
						        src={myImage3}
						        width = "2040"
						        height = "608"
						        className="d-inline-block w-100 image-fluid"
						        />
					      </Carousel.Item>
				    	</Carousel>
	 				</Col>
	 			</Row>

	 			<Row>
	 				<Col className="py-5 text-center">
	 					<h1 className="NavbarBrand-text"> Yamaha Make Waves </h1>
	 					<h4 className="paraFont"> Show Now ! </h4>
	 				</Col>
	 			</Row>
 		</Container>
 
	
	)
}