import React, { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import ViewCart from '../components/ViewCart';

export default function CartUser() {
    const { user } = useContext(UserContext);
    const [cartData, setCartData] = useState([]);

    useEffect(() => {
        if (user.id) {
            fetch(`https://capstone2-easypc.onrender.com/api/users/checkout`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                },
            })
            .then(response => response.json())
            .then(data => {
                setCartData(data.cart);
            })
            .catch(error => {
                console.error('Error fetching cart data:', error);
            });
        }
    }, [user.id]);

    return <ViewCart cartData={cartData} />;
}