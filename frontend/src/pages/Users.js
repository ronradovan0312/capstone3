import { useContext, useEffect, useState } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import AdminViewUsers from '../components/AdminViewUsers';

export default function Users() {
const {user} = useContext(UserContext);
  const [users, setUsers] = useState([]);

  const fetchUsers = () => {
    fetch(`https://capstone2-easypc.onrender.com/api/users/all`)
      .then(response => response.json())
      .then(result => {
        setUsers(result);
      })
      .catch(error => {
        console.error('Error fetching users:', error);
      });
  };

  useEffect(() => {
    fetchUsers();
  }, []);

  return (
     
      (user.isAdmin === true) ?
              <AdminViewUsers usersData={users} fetchUsers={fetchUsers}/> 
          : 
              <Navigate to='/' />   
    
  )
}