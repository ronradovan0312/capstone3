import React, { useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView.js';
import UserContext from '../UserContext.js';
import ViewCart from '../components/ViewCart.js';

export default function Products() {
    const { user } = useContext(UserContext);
    const [products, setProducts] = useState([]);

    const fetchProducts = () => {
        fetch(`https://capstone2-easypc.onrender.com/api/products/allProducts`)
        .then(response => response.json())
        .then(result => {
            setProducts(result);
        })
    }

    useEffect(() => {
        fetchProducts();
    }, []);
    
    return (
        <div>
            <h1>Product List</h1>
            <ul>
                {products.map(product => (
                    <li key={product.id}>
                        {product.name} - ${product.price}
                    </li>
                ))}
            </ul>
        </div>
    )
}
