import { useContext, useEffect, useState } from 'react';
import { Button, Card, Carousel, Col, Container, Row } from 'react-bootstrap';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';
import AddToCart from '../components/AddToCart.js';
import CreateOrderModal from '../components/CreateOrderModal';


export default function ProductItem({UsersData, product}){

	// The use Params hook will allow us to access the ID of the course from the URL Parameters
	const {productId} = useParams();


	const{user} = useContext(UserContext);

	//initializing the useNavigate hook
	const navigate = useNavigate(); 

	const[name, setName] = useState(""); 
	const[description, setDescription] = useState(""); 
	const[price, setPrice] = useState(0); 
    const[image, setImage] = useState('')
    const[quantity, setQuantity] = useState(0)


    const loginFirst = () => {
        if(user.id === null){
            Swal.fire({
                title: "Log into your account first",
                icon: 'warning'
            })
        }
    }

	useEffect(() => {
		fetch(`https://capstone2-easypc.onrender.com/api/products/${productId}`)
		.then(response => response.json())
		.then(result => {
            setImage(result.image)
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
            setQuantity(result.quantity)
		})
	}, [productId])

	return(
       (user.isAdmin === true)?
            <Navigate to='/products' />
            :
            <>
            <Container>
                 <Carousel> 
                    <Carousel.Item>
                        <Container className='d-flex align-items-center pt-5'>
                                <Row className='d-flex justify-content-center'>
                                    <Col xs={12} md={4} className="m-3"> 
                                        <Card className="cardHighlight px-3 carousel-control">
                                        <Card.Img variant="top" 
                                            src={image}
                                            width = "500"
                                            height = "500"
                                            className="my-5 w-100 h-50 image-fluid carousel-control"
                                            />
                                        </Card>
                                    </Col>

                                    <Col xs={12} md={5} className="m-3 py-5">
                                        <Card className="cardHighlight p-2 carousel-control">
                                            <Card.Body className="text-center cardBody">
                                                <Card.Title className='NavbarBrand-text py-3'> <h1> {name}  </h1></Card.Title>

                                                <Card.Subtitle> Description: </Card.Subtitle>
                                                <Card.Text className='paraFont'> {description} </Card.Text>

                                                <Card.Subtitle> Price: </Card.Subtitle>
                                                <Card.Text className='paraFont'> Php: {price} </Card.Text>
                                            
                                                {(user.id !== null) ?
                                                    <>
                                                        <AddToCart product_id={productId}
                                                        />
                                                        <CreateOrderModal product_id={productId} />
                                                    </>
                                                    :
                                                    <>
                                                        <Button className='m-2' variant="outline-dark" onClick={() => loginFirst()}> Add to cart </Button>
                                                        <Button className='m-2' variant="outline-dark" onClick={() => loginFirst()}> Order </Button>
                                                    </>
                                                }
                        
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                </Row>
                        </Container>
                </Carousel.Item>
            </Carousel>
        </Container>
           
     </>
	)
}
