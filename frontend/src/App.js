import './App.css';
import AppNavbar from './components/AppNavbar.js';
import Register from './components/Register.js';
import Login from './components/Login.js';
import Logout from  './pages/Logout.js';
import Home from './pages/Home.js';
import Products from './pages/Products.js';
import Users from './pages/Users';
import Orders from './pages/Orders';
import AddProduct from './components/AddProduct.js';
import ProductItem from './pages/ProductItem';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { useState, useEffect} from 'react';
import {UserProvider} from './UserContext.js'; 
import { ModalProvider } from './ModalContext';

function App() {
    const [user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`https://capstone2-easypc.onrender.com/api/users/details`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
          id: localStorage.getItem('userId')
      })
    })
    .then(response => response.json())
    .then(result => {

      if(typeof result._id !== 'undefined'){
        // even if we refresh the browser the 'user' state will still have its values re-assigned.
        setUser({
          id: result._id,
          isAdmin: result.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }

    })
    .catch(error => {
      console.log(error);
      alert("An error occurred during login.");
    });
  }, [])

  return (
   <>
     <UserProvider value={{user, setUser, unsetUser}}> 
        <Router>
          <ModalProvider>
                <AppNavbar/>
                <AddProduct/>
                <Register/>
                <Login/>
          </ModalProvider>
          <Container>
              <Routes>
                 <Route  path='/' element={<Home/>} />
                 <Route  path='/products' element={<Products/>}/>
                 <Route  path='/products/:productId' element={<ProductItem/>}/>
                 <Route path='/login' element={<Login/>}/>
                 <Route path='/logout' element={<Logout/>}/>
                 <Route path='/register' element={<Register/>}/>
                 <Route path='/users' element={<Users/>}/>
                 <Route path='/orders' element={<Orders/>}/>
              </Routes>
          </Container>
        </Router>
     </UserProvider>
   </>
  );
}

export default App;
